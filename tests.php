<?php

namespace phpBench\tests;

abstract class Container
{
    public static function test1(... $args)
    {
        return json_encode($args, JSON_PRETTY_PRINT);
    }

    public static function test2(... $args)
    {
        return serialize($args);
    }
}

abstract class System
{
    /**
     * Standard; Payload generator
     * Logic should be adjusted to specific needs.
     *
     * @param array ...$args
     * @return array
     */
    public static function payload(... $args)
    {
        $array = [];
        $size = mt_rand(10, 20);
        for ($i = 0; $i < $size; $i++) {
            $array[] = [range(1000, 2000), str_split(bin2hex(random_bytes(1024)), 2)];
        }
        return $array;
    }

    /**
     * Standard; Settings
     * Describes settings for specific test method
     *
     * @return array
     */
    public static function settings()
    {
        return [
            "subject" => "Array manipulation (json_encode vs serialize)",
            "tests" => [
                "methodName" => [
                    "iterations" => 100
                ]
            ]
        ];
    }


    /**
     * Magic method
     * If called method undefined then throw custom error
     *
     * @param $name
     * @param $arguments
     */
    public static function __callStatic($name, $arguments)
    {
        array_walk($arguments, function (&$value) use (&$arguments_detailed) {
            $value = gettype($value) . " " . ((is_array($value)) ? "" : $value);
        });

        $output = "Required method is missing: '{$name}'" . PHP_EOL;
        $output .= "Details - " . self::class . "::{$name}(" . implode(', ', $arguments) . ")" . PHP_EOL;
        trigger_error($output, E_USER_ERROR);
    }

}

