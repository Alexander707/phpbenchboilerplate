<?php

namespace phpBench;
require "tests.php";

use phpBench\tests\System;

$targetClass = 'phpBench\tests\Container';
$register = get_class_methods($targetClass);
$payload = System::payload();
$settings = System::settings();

echo "Benchmark running on: PHP " . phpversion() . PHP_EOL;
echo "Subject: {$settings['subject']}" . PHP_EOL;

if (is_array($register)) {
    echo "Running..." . PHP_EOL;
    foreach ($register as $id => $testMethod) {

        if ($testMethod === 'payload' || $testMethod === 'settings') {
            continue;
        }

        $startTime = microtime(true);

        if (isset($settings['tests'][$testMethod])) {
            $iterations = $settings[$testMethod]['iterations'] ?? 10000;
        } else {
            $iterations = 10000;
        }

        for ($i = 0; $i < $iterations; $i++) {
            $new = call_user_func([$targetClass, $testMethod], [$payload]);
            $new = call_user_func([$targetClass, $testMethod], [$payload, $new]);
            usleep(5);
        }


        $endTime = microtime(true);

        echo $testMethod . "() took " . ($endTime - $startTime) . " microseconds to complete." . PHP_EOL;
        unset($startTime, $endTime, $testMethod);
    }
} else {
    echo "Terminated. Couldn't load tests!" . PHP_EOL;
}

exit;